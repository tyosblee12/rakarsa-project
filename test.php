 <!-- AWAL NAVBAR -->
 <?php include 'template/navbar.php';?>
 <!-- END OF NAVBAR -->

 <div class="containter-fluid">
     <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
             <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
             <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
             <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
         </ol>
         <div class="carousel-inner">
             <div class="carousel-item active">
                 <h1 class="head text-white">Rakarsa Foundation</h1>
                 <img class="d-block w-100" src="assets/img/unsplash/eberhard-grossgasteiger-y2azHvupCVo-unsplash.jpg"
                     alt="First slide">
                 <div class="carousel-caption d-none d-md-block">
                     <a class="btn btn-blues btn-rounded"> Explore </a>
                 </div>
             </div>
             <div class="carousel-item">
                 <img class="d-block w-100" src="assets/img/unsplash/fiqry-choerudin-m_ssaxSvnDk-unsplash.jpg"
                     alt="Second slide">
                 <div class="carousel-caption d-none d-md-block">
                     <a class="btn btn-blues btn-rounded"> Explore </a>
                 </div>
             </div>
             <div class="carousel-item">
                 <img class="d-block w-100" src="assets/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg"
                     alt="Third slide">
                 <div class="carousel-caption d-none d-md-block">
                     <a class="btn btn-blues btn-rounded"> Explore </a>
                 </div>
             </div>
         </div>
         <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
             <span class="carousel-control-prev-icon" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
             <span class="carousel-control-next-icon" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
         </a>
     </div>
 </div>




 <!-- AWAL NAVBAR -->
 <?php include 'template/footer.php';?>
 <!-- END OF NAVBAR -->