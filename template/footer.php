</body>
<footer class="footer text-dark fixed-bottom">
    <div class="container">
        <div class="col-md-12 col-lg-12">
            <span class="text-sm float-right">&copy 2020 - <?php echo date("Y"); ?> Yayasan Rodha Among Karsa - Rakarsa
                Foundation
                | Design by <strong><a class="stretched-link text-blues" href="https://instagram.com/tyosblee12"> ZACD
                    </a>
                </strong></span>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js"></script>
<script>
gsap.from('.navbar', {
    duration: 2,
    y: '-100%',
    opacity: 0,
    ease: 'bounce'
});
gsap.from('.container figure', {
    duration: 1,
    delay: 1,
    y: '-100%',
    opacity: 0,
    ease: 'elastic.out(1, 0.3)'
})
gsap.from('.lightbox', {
    opacity: 0,
    duration: 1.5,
    delay: 1,
})
gsap.from('#active-member img', {
    duration: 1.2,
    delay: 1,
    y: '-100%',
    opacity: 0,
    ease: "back.out(2,8)"
})
gsap.from('p', {
    duration: 1.2,
    y: '-100%',
    opacity: 0,
});
gsap.from('#judul', {
    duration: 1.5,
    y: '-100%',
    opacity: 0,
});
</script>

</html>

</html>