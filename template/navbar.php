<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rakarsa Foundation</title>
    <link rel="stylesheet" href="assets/css/mystyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"
        integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg=="
        crossorigin="anonymous" />
    <!-- <script src="https://unpkg.com/scrollreveal"></script> -->

</head>
<?php
$CurPageURL =  $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];    
if(($CurPageURL == "rakarsa.org/") or ($CurPageURL == "localhost/rakarsa/")){
    $home ="active";
}elseif(($CurPageURL == "rakarsa.org/about.php") or ($CurPageURL == "localhost/rakarsa/about.php")){
    $about ="active";
}elseif(($CurPageURL == "rakarsa.org/project.php") or ($CurPageURL == "localhost/rakarsa/project.php")){
    $pro ="active";
}elseif(($CurPageURL == "rakarsa.org/index.php") or ($CurPageURL == "localhost/rakarsa/index.php")){
    $home="active";
}elseif(($CurPageURL == "rakarsa.org/rubicon.php") or ($CurPageURL == "localhost/rakarsa/rubicon.php")){
    $cross="active";
}
?>
<nav class="navbar navbar-expand-md navbar-light fixed-top shadow-sm pt-3">
    <div class="container">
        <a href="index.php" class="navbar-brand">
            <img src="assets/img/aset/RAKARSAFoundation-Logo-01.png" height="35" width="100" alt="LOGO">
        </a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
            <div class="navbar-nav">
                <a href="index.php" class="nav-item nav-link <?php echo $home ?>">home</a>
                <a href="about.php" class="nav-item nav-link <?php echo $about ?>">about</a>
                <a href="project.php" class="nav-item nav-link <?php echo $pro ?>">project</a>
                <a href="rubicon.php" class="nav-item nav-link <?php echo $cross ?>">cross the rubicon</a>
            </div>
        </div>
    </div>
</nav>