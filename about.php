<!-- Awal Navbar -->
<?php include ('template/navbar.php'); ?>
<!-- Akhir Navbar -->

<!-- Awal Konten -->

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pr-5">
                <div class="h4 text-blues pb-5" id="judul">about us</div>
                <div class="row">
                    <div class="col-lg-5 pr-5">
                        <p id="kiri">Rakarsa Foundation (Yayasan Rodha Among Karsa) awalnya didirikan untuk
                            mendukung eksperimen sistem ekonomi koperasi pekerja seni kontemporer di
                            Bandung. Pandemi Covid-19 kemudian mengharuskan penataan ulang koperasi,
                            sementara yayasan mengembangkan tujuan strategisnya. Tahun 2020, yayasan
                            berupaya mengembangkan jaringan berbagi pengetahuan interdisipliner sebagai
                            upaya mengembangkan kontribusi praktik artistik terhadap perkembangan budaya,
                            sosial dan ekologi. Fokus program kami adalah: 1) inisiatif dan manajemen
                            pameran, 2) inisiatif edukasi dan berbagi pengetahuan. </p>
                    </div>
                    <div class="col-lg-5 pr-5">
                        <p id="kanan">Rakarsa Foundation (Yayasan Rodha Among Karsa) was initially
                            established as a support on cooperative economic ecosystem for Bandung
                            contemporary art workers. As the cooperative undergoes a reassessment, the
                            foundation continued to develop its strategic objectives. In 2020, the
                            foundation seeks to develop interdisciplinary knowledge-sharing networks to
                            promote the contribution of artistic practice to cultural, social and ecological
                            developments. Our programming focus including: 1) exhibition initiative and
                            management, 2) education and knowledge-sharing initiatives.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="h4 mb-3 text-blues" id="judul">active members</div>
                <div class="row p-0" id="active-member">
                    <div class="col-lg-3 p-4">
                        <img src="assets/img/avatar/FotoProfil_Vincent.jpg" alt="Vincent Rumahloine"
                            class="img-fluid rounded-circle text-center" width="120">
                        <div class="h4 mb-3 mt-3 font-weight-700" id="member">Vincent Rumahloine</div>
                        <p class="card-text" id="kiri">
                            adalah seniman kontemporer yang mengolah jalinan sosial dan keseharian
                            masyarakat. Karyanya banyak mengangkat manusia; dari isu sosial, hak, nilai
                            tradisi, relasi, memori kolektif hingga mereka ulang narasi sejarah. Proyeknya
                            telah dipamerkan di Pulosari Public Space (Bandung), Contemporary Ujazdowski
                            Castle in Warsaw (Poland), dan Galeri Nasional Indonesia (Jakarta) Ia menjadi
                            seniman terbaik di Bandung Contemporary Art Awards tahun 2019.</p>
                        <p class="card-text" id="kanan">
                            is a contemporary artist who playfully interrogates the social
                            fabric and the mundane in society. His work mainly revolves around
                            people; from social issues, human rights, traditional values, human
                            relations, collective memories to re-designing historic narratives.
                            His project has been presented at Pulosari Public Space in Bandung,
                            Contemporary Ujazdowski Castle in Warsaw (Poland), and The National
                            Gallery of Indonesia in Jakarta. He won the first prize in Bandung
                            Contemporary Art Awards in 2019.</p>
                    </div>
                    <div class="col-lg-3 p-4">
                        <img src="assets/img/avatar/FotoProfil_Endira.jpg" alt="Endira F. Julianda"
                            class="img-fluid rounded-circle" width="120">
                        <div class="h4 mb-3 mt-3 font-weight-700" id="member">Endira F. Julianda</div>
                        <p class="card-text" id="kiri">
                            adalah seniman rupa yang tertarik dengan interelasi antar manusia, non-manusia,
                            ruang dan lingkungan; yang juga terefleksi dalam perhatiannya pada ekosistem
                            seni rupa kontemporer. Karyanya telah dipamerkan pada pameran kelompok dan
                            tunggal di Indonesia, Singapura, Filipina, Taiwan dan Jerman. Endira juga
                            terlibat sebagai direktur artistik dalam berbagai proyek desain, musik, film,
                            dan manajemen inisiatif seni dan budaya. Ia sebelumnya merancang program untuk
                            Platform3 (2014-2017), Bandung Contemporary Art Awards (2017, 2019), dan turut
                            mendirikan Rakarsa Foundation dan Koperasi.</p>
                        <p class="card-text" id="kanan">
                            is a visual artist who observes ecological aspects of human lives;
                            through spaces humans and non-humans occupy and the dynamics within
                            the space. Her works have been exhibited in solo and group
                            exhibitions in Indonesia, Singapore, Philippines, Taiwan and
                            Germany. Endira also engages as art director in product and graphic
                            design, music, feature films, as well as promoting visual art and
                            cultural activities. She managed programs for Platform3 (2014-2017),
                            directed Bandung Contemporary Art Awards (2017, 2019), and co-found
                            Rakarsa Foundation and Coop</p>
                    </div>
                    <div class="col-lg-3 p-4">
                        <img src="assets/img/avatar/FotoProfil_Yori.jpg" alt="Vincent Rumahloine"
                            class="img-fluid rounded-circle" width="120">
                        <div class="h4 mb-3 mt-3 font-weight-700" id="member">Yori Papilaya</div>
                        <p class="card-text" id="kiri">
                            memiliki pengalaman sebagai pengelola keuangan pada sebuah ruang seni komersial
                            di Bandung. Sebelumnya ia adalah ilustrator utama di Oray Studios, Bandung
                            selama 11 tahun. Ketertarikannya pada dunia kreatif mengantarkan pada kolaborasi
                            seperti film dengan Kutub Tiga, komik dengan Pakoban, ilustrasi dan animasi
                            dengan Sembilan Matahari, juga terhadap fashion dan literatur. Ia telah
                            menerbitkan tulisan di Jurnal Karbon, menjadi editor karya fiksi Karim Nas
                            ‘Puspabangsa’ dan menerbitkan karya fiksinya ‘Dances of The Past’. </p>
                        <p class="card-text" id="kanan">
                            currently works as financial officer for ArtSociates, Lawangwangi
                            Bandung. He was a lead illustrator at Oray Studios, Bandung for 11
                            years. His wide interests in the creative field led him to
                            collaborate in many various projects, such as filmmaking with Kutub
                            Tiga, comics with Pakoban, illustration and animation with Sembilan
                            Matahari, also in fashion and writing. He has published his writing
                            in Jurnal Karbon, edited Karim Nas’ fiction ‘Puspabangsa’ as well as
                            publishing a fictional work “Dances of The Past”.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include ('template/footer.php'); ?>
</body>