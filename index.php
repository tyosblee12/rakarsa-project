    <!-- AWAL NAVBAR -->
    <?php include 'template/navbar.php';?>
    <!-- END OF NAVBAR -->
    <!-- <div class="containter-fluid mb-5">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <h1 class="head text-white">Rakarsa Foundation</h1>
                    <img class="d-block w-100"
                        src="assets/img/unsplash/eberhard-grossgasteiger-y2azHvupCVo-unsplash.jpg" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        <a class="btn btn-blues btn-rounded"> Explore </a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="assets/img/unsplash/fiqry-choerudin-m_ssaxSvnDk-unsplash.jpg"
                        alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                        <a class="btn btn-blues btn-rounded"> Explore </a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="assets/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg"
                        alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                        <a class="btn btn-blues btn-rounded"> Explore </a>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div> -->

    <div class="container pt-5 mt-5" id="fungsi">
        <div class="row">
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-6">
                        <p id="kiri">Rakarsa Foundation adalah organisasi non-profit asal Bandung yang berupaya
                            mewujudkan ekosistem seni kontemporer yang berkelanjutan. Fokus kami adalah mengembangkan
                            jaringan berbagi pengetahuan dalam semangat artistik melalui pameran, acara publik, dan
                            inisiatif edukasional.</p>
                    </div>
                    <div class="col-lg-6" id="active-member">
                        <p id="kanan">Rakarsa Foundation is a Bandung based non-profit organization that promotes
                            sustainable contemporary arts ecosystems. We concern in expanding the network of shared
                            knowledge within the artistic milieu through exhibitions, public events, and educational
                            initiatives.</p>
                    </div>
                </div>
                <div class="h4 pb-5" id="judul">on going project</div>

                <a href="https://www.goethe.de/ins/id/id/sta/ban/ver/rubicon.html">
                    <figure class="figure" id="figur1">
                        <img src="assets/img/aset/web-banner.jpeg" class="figure-img img-fluid rounded"
                            alt="A generic square placeholder image with rounded corners in a figure.">
                    </figure>
                </a>
                <div class="row">
                    <div class="col-lg">
                        <p id="kiri">Rubicon menawarkan wadah untuk menemukan bentuk interdisipliner dari
                            solidaritas yang berkelanjutan dan praktik kolaboratif. Program ini berlangsung dari
                            Januari-Desember 2021, merupakan hasil kerja sama dari Goethe-Institut Bandung, Institut
                            Français Indonesia, dan Rakarsa Foundation, yang didukung oleh Deutsch-Französischer
                            Kulturfonds.</p>
                    </div>
                    <div class="col-lg">
                        <p id="kanan">Rubicon offers a platform to invent interdisciplinary forms of sustainable
                            solidarity and new
                            practices of collaboration. The program runs from January to December 2021 as a
                            collaboration of Goethe-Institut Bandung, Institut Français Indonesia, and Rakarsa
                            Foundation, supported by Deutsch-Französischer Kulturfonds.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container" id="fungsi">
            <div class="row pb-5 text-dark">
                <div class="col-lg-12 pt-5 mt-5">
                    <p> <i class="fa fa-phone text-dark"></i> &nbsp (+62) 8179 255 915</p>
                    <p> <i class="fa fa-envelope text-dark"></i> &nbsp contact@rakarsa.org</p>
                    <p> <i class="fa fa-instagram"></i> &nbsp @ra.kar.sa</p>
                </div>
            </div>
        </div>
    </div>
    <section id="footer">
        <?php include 'template/footer.php';?>
    </section>