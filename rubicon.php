<!-- AWAL NAVBAR -->
<?php include 'template/navbar.php';?>
<!-- END OF NAVBAR -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Newsreader:wght@400;700&display=swap" rel="stylesheet">

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col rubicon">
                <div class="h4 text-center pb-4" id="judul">Cross The Rubicon</div>
                <p class="pb-3 pt-0 text-center">Yori Papilaya</p>
                <p>The point of no return represents the precise spot in a story where the major character takes
                    a new
                    and unplanned direction. They make a decision or take an action that is irrevocable. The
                    only
                    possible action is moving forward, no matter the level of preparedness.</br>
                </p>
                <p>
                    I feel like we're living in an exciting time, from where there is no return. We always
                    consider
                    ourselves as the protagonists in our own stories, without thinking that a story needs a
                    conflict to
                    make it interesting. It was upsetting at first, seeing how everything you thought as the
                    norm were
                    suddenly deemed abnormal. It's a funny feeling, to experience the world suddenly turned
                    upside down.
                    </br>
                </p>
                <p>
                    In stories, there always an ending. Whether or not it's a happy one, that's irrelevant. Our
                    mind
                    soothed by the fact that there will be an ending at the last pages of the book or last
                    minutes of
                    the movie. But when the point happened in real life, we get all dumbfounded. We don't know
                    when and
                    how this dream (or nightmare) will end. I feel this is how it felt like to be the
                    protagonist in a
                    story, when they came across the middle of the story. We never really understand their
                    struggles
                    until now. And it's horrifying.
                    </br>
                </p>
                <p>
                    So which is it? Is it exciting, or horrifying? Well, both. Being horrified is exciting, and
                    being
                    excited is horrifying. It's a same sensations, from different perspectives. We're the ones
                    who
                    control our own perspectives, and that's empowering.
                </p>
                <p class="text-center font-weight-bold pt-3 text-dark">Rubicon~ runs from January-December 2021.
                    More info <a class="font-weight-bold"
                        href="https://www.goethe.de/ins/id/en/sta/ban/ver/rubicon.html">
                        here</a></p>
            </div>
            <div class="col-lg-2"></div>

        </div>
        <section id="footer">
            <?php include 'template/footer.php';?>
        </section>
</body>