<?php
include "../config.php";
$email = $_SESSION['email'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE email = '$email'");
$data = mysqli_fetch_array($nama_user);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Dashboar &mdash; Rakarsa</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Libraries -->

    <!-- Template CSS -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/components.css">
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <form class="form-inline mr-auto">
                    <ul class="navbar-nav mr-3">
                        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i
                                    class="fas fa-bars"></i></a></li>
                        <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                                    class="fas fa-search"></i></a></li>
                    </ul>
                </form>
                <ul class="navbar-nav navbar-right">
                    <li class="dropdown"><a href="#" data-toggle="dropdown"
                            class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <img alt="image" src="../assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
                            <div class="d-sm-none d-lg-inline-block">Hi, <?= $data['nama']; ?></div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-title">Logged in 5 min ago</div>
                            <a href="features-profile" class="dropdown-item has-icon">
                                <i class="far fa-user"></i> Profile
                            </a>
                            <a href="features-activities" class="dropdown-item has-icon">
                                <i class="fas fa-bolt"></i> Activities
                            </a>
                            <a href="features-settings" class="dropdown-item has-icon">
                                <i class="fas fa-cog"></i> Settings
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="../logout.php" class="dropdown-item has-icon text-danger">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="main-sidebar">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="index.php">Rakarsa Foundation</a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="index.php">RF</a>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="menu-header">Dashboard</li>
                        <li class="nav-item dropdown active">
                            <a href="index.php" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                        </li>
                        <li class="menu-header">Post</li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i
                                    class="fas fa-columns"></i> <span>Layout</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="">Default Layout</a></li>
                                <li><a class="nav-link" href="">Transparent Sidebar</a></li>
                                <li><a class="nav-link" href="">Top Navigation</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i>
                                <span>Bootstrap</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="#">Alert</a></li>
                                <li><a class="nav-link" href="#">Badge</a></li>
                                <li><a class="nav-link" href="#">Breadcrumb</a></li>
                                <li><a class="nav-link" href="#">Buttons</a></li>
                                <li><a class="nav-link" href="#">Card</a></li>
                                <li><a class="nav-link" href="#">Carousel</a></li>
                                <li><a class="nav-link" href="#">Collapse</a></li>
                                <li><a class="nav-link" href="#">Dropdown</a></li>
                                <li><a class="nav-link" href="#">Form</a></li>
                                <li><a class="nav-link" href="#">List Group</a></li>
                                <li><a class="nav-link" href="#">Media Object</a></li>
                                <li><a class="nav-link" href="#">Modal</a></li>
                                <li><a class="nav-link" href="#">Nav</a></li>
                                <li><a class="nav-link" href="#">Navbar</a></li>
                                <li><a class="nav-link" href="#">Pagination</a></li>
                                <li><a class="nav-link" href="#">Popover</a></li>
                                <li><a class="nav-link" href="#">Progress</a></li>
                                <li><a class="nav-link" href="#">Table</a></li>
                                <li><a class="nav-link" href="#">Tooltip</a></li>
                                <li><a class="nav-link" href="#">Typography</a></li>
                            </ul>
                        </li>
                        <li class="menu-header">Stisla</li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-th-large"></i>
                                <span>Components</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="#">Article</a></li>
                                <li><a class="nav-link beep beep-sidebar" href="#">Avatar</a></li>
                                <li><a class="nav-link" href="#">Chat Box</a></li>
                                <li><a class="nav-link beep beep-sidebar" href="#">Empty State</a></li>
                                <li><a class="nav-link" href="#">Gallery</a></li>
                                <li><a class="nav-link beep beep-sidebar" href="#">Hero</a></li>
                                <li><a class="nav-link" href="#">Multiple Upload</a></li>
                                <li><a class="nav-link beep beep-sidebar" href="#">Pricing</a>
                                </li>
                                <li><a class="nav-link" href="#">Statistic</a></li>
                                <li><a class="nav-link" href="#">Tab</a></li>
                                <li><a class="nav-link" href="#">Table</a></li>
                                <li><a class="nav-link" href="#">User</a></li>
                                <li><a class="nav-link beep beep-sidebar" href="#">Wizard</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i>
                                <span>Forms</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="forms-advanced-form">Advanced Form</a></li>
                                <li><a class="nav-link" href="forms-editor">Editor</a></li>
                                <li><a class="nav-link" href="forms-validation">Validation</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-map-marker-alt"></i> <span>Google
                                    Maps</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="gmaps-advanced-route">Advanced Route</a></li>
                                <li><a href="gmaps-draggable-marker">Draggable Marker</a></li>
                                <li><a href="gmaps-geocoding">Geocoding</a></li>
                                <li><a href="gmaps-geolocation">Geolocation</a></li>
                                <li><a href="gmaps-marker">Marker</a></li>
                                <li><a href="gmaps-multiple-marker">Multiple Marker</a></li>
                                <li><a href="gmaps-route">Route</a></li>
                                <li><a href="gmaps-simple">Simple</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-plug"></i>
                                <span>Modules</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="modules-calendar">Calendar</a></li>
                                <li><a class="nav-link" href="modules-chartjs">ChartJS</a></li>
                                <li><a class="nav-link" href="modules-datatables">DataTables</a></li>
                                <li><a class="nav-link" href="modules-flag">Flag</a></li>
                                <li><a class="nav-link" href="modules-font-awesome">Font Awesome</a></li>
                                <li><a class="nav-link" href="modules-ion-icons">Ion Icons</a></li>
                                <li><a class="nav-link" href="modules-owl-carousel">Owl Carousel</a></li>
                                <li><a class="nav-link" href="modules-sparkline">Sparkline</a></li>
                                <li><a class="nav-link" href="modules-sweet-alert">Sweet Alert</a></li>
                                <li><a class="nav-link" href="modules-toastr">Toastr</a></li>
                                <li><a class="nav-link" href="modules-vector-map">Vector Map</a></li>
                                <li><a class="nav-link" href="modules-weather-icon">Weather Icon</a></li>
                            </ul>
                        </li>
                        <li class="menu-header">Pages</li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Auth</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="auth-forgot-password">Forgot Password</a></li>
                                <li><a href="auth-login">Login</a></li>
                                <li><a class="beep beep-sidebar" href="auth-login-2">Login 2</a></li>
                                <li><a href="auth-register">Register</a></li>
                                <li><a href="auth-reset-password">Reset Password</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-exclamation"></i>
                                <span>Errors</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="errors-503">503</a></li>
                                <li><a class="nav-link" href="errors-403">403</a></li>
                                <li><a class="nav-link" href="errors-404">404</a></li>
                                <li><a class="nav-link" href="errors-500">500</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-bicycle"></i>
                                <span>Features</span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="features-activities">Activities</a></li>
                                <li><a class="nav-link" href="features-post-create">Post Create</a></li>
                                <li><a class="nav-link" href="features-posts">Posts</a></li>
                                <li><a class="nav-link" href="features-profile">Profile</a></li>
                                <li><a class="nav-link" href="features-settings">Settings</a></li>
                                <li><a class="nav-link" href="features-setting-detail">Setting Detail</a></li>
                                <li><a class="nav-link" href="features-tickets">Tickets</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i>
                                <span>Utilities</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="utilities-contact">Contact</a></li>
                                <li><a class="nav-link" href="utilities-invoice">Invoice</a></li>
                                <li><a href="utilities-subscribe">Subscribe</a></li>
                            </ul>
                        </li>
                        <li><a class="nav-link" href="credits"><i class="fas fa-pencil-ruler"></i>
                                <span>Credits</span></a></li>
                    </ul>

                    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                        <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
                            <i class="fas fa-rocket"></i> Documentation
                        </a>
                    </div>
                </aside>
            </div>