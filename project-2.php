<?php include ('template/navbar.php'); ?>

<body>
    <div class="containter-fluid mt-5">
        <div class="container mt-5">
            <div class="row pt-5">
                <div class="col-lg-2">
                    <div class="navbar-nav justify-content-center sticky-sidebar">
                        <a href="#section1" class="nav-item nav-link text-dark font-weight-bold">Lorem, ipsum . 1</a>
                        <a href="#section2" class="nav-item nav-link text-dark font-weight-bold">Lorem, ipsum . 2</a>
                        <a href="#section3" class="nav-item nav-link text-dark font-weight-bold">Lorem, ipsum . 3</a>
                        <a href="#section4" class="nav-item nav-link text-dark font-weight-bold">Lorem, ipsum . 4</a>
                    </div>
                </div>
                <div class="col-lg-10" id="kiri">
                    <?php $judul = 1 ?>
                    <div class="col-lg-12 pb-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <figure class="figure" id="figur2">
                                    <img src="assets/img/unsplash/boxed-water-is-better-jbcY_yvsDzk-unsplash.jpg"
                                        class="figure-img img-fluid rounded post"
                                        alt="A generic square placeholder image with rounded corners in a figure.">
                                    <figcaption class="figure-caption">A caption for the above image.</figcaption>
                                </figure>
                            </div>
                            <div class="col-lg-6">
                                <div class="h4 pb-3" id="judul">Lorem, ipsum dolor. <?= $judul++ ?></div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet tempore, doloribus
                                    sequi itaque est eos. Unde vero doloremque odit totam ad mollitia, accusamus velit,
                                    officia harum sint aliquid eligendi tenetur? Dolore vitae molestiae culpa aliquid.
                                    Officiis, adipisci ad tenetur, culpa pariatur porro totam eveniet odit tempore fugit
                                    molestias placeat fugiat?</p>
                                <a class="btn btn-blues text-white btn-sm"> Baca Selengkapnya ... </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 pb-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <figure class="figure" id="figur2">
                                    <img src="assets/img/unsplash/eberhard-grossgasteiger-y2azHvupCVo-unsplash.jpg"
                                        class="figure-img img-fluid rounded post"
                                        alt="A generic square placeholder image with rounded corners in a figure.">
                                    <figcaption class="figure-caption">A caption for the above image.</figcaption>
                                </figure>
                            </div>
                            <div class="col-lg-6">
                                <div class="h4 pb-3" id="judul">Lorem, ipsum dolor. <?= $judul++ ?></div>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam recusandae accusamus
                                    sunt tempore. Dolorum mollitia cupiditate temporibus asperiores nam vel, iusto sequi
                                    assumenda accusantium. Reprehenderit qui laudantium debitis laboriosam! Aliquid sunt
                                    doloremque veritatis dicta, dignissimos, soluta inventore voluptatum ab cumque hic
                                    veniam sequi ex atque labore vitae distinctio maxime illum?</p>
                                <a class="btn btn-blues text-white btn-sm"> Baca Selengkapnya ... </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 pb-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <figure class="figure" id="figur2">
                                    <img src="assets/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg"
                                        class="figure-img img-fluid rounded post"
                                        alt="A generic square placeholder image with rounded corners in a figure.">
                                    <figcaption class="figure-caption">A caption for the above image.</figcaption>
                                </figure>
                            </div>
                            <div class="col-lg-6">
                                <div class="h4 pb-3" id="judul">Lorem, ipsum dolor. <?= $judul++ ?></div>
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolor tenetur cumque
                                    magnam. Ut enim, laboriosam dolorum magnam sed quaerat adipisci aperiam, eos iste
                                    magni earum rerum corporis consequuntur beatae pariatur unde placeat? Quibusdam
                                    accusamus harum eius earum cupiditate maxime nihil cumque omnis unde ducimus libero
                                    fuga corrupti, facere, aspernatur quos.</p>

                                <a class="btn btn-blues text-white btn-sm"> Baca Selengkapnya ... </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include ('template/footer.php'); ?>
</body>