<?php include ('template/navbar.php'); ?>

<!-- CSS GALERY -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
<link rel="stylesheet" href="assets/css/compact-gallery.css">

<body>

    <div class="container">
        <div class="h4" id="judul">previous project</div>
        <section class="gallery-block compact-gallery">
            <div class="row">
                <div class="col-lg-4">
                    <div class="item zoom-on-hover">
                        <a class="lightbox" href="assets/img/aset/Rakarsa-LPPM-1.jpg">
                            <img class="img-fluid image-atas" src="assets/img/aset/Rakarsa-LPPM-1.jpg"
                                alt="Workshops & Trainings">
                        </a>
                    </div>
                    <div class="h4 mb-3 mt-3" id="judul">Workshops & Trainings</div>
                    <p class="card-text" id="kiri">In 2019, Rakarsa worked with LPPM ITB for community based workshops
                        and
                        trainings for craft and
                        applied art in Desa Ciburial, Bandung. The community has been working with textile, bamboo and
                        wood
                        waste previously. The trainings were tailored to apply artistic and design approach in creating
                        products, as well as preparing products for online distribution.</p>
                    <p class="card-text" id="kiri">Lokakarya lainnya termasuk Book Binding bersama Irfan Hendrian
                        (2019),
                        dan Basic Pottery Technique & Mug Making dengan Tacit Ceramics (2019). </p>
                    <p class="card-text" id="kanan">Rakarsa worked with LPPM ITB for community based workshops and
                        trainings
                        for craft and applied art in Desa Ciburial, Bandung in 2019. The community has been working with
                        textile, bamboo and wood waste previously. The trainings were tailored to apply artistic and
                        design
                        approach in creating products, as well as preparing products for online distribution. Other
                        workshops are Book Binding by Irfan Hendrian (2019) and Basic Pottery Technique & Mug Making
                        with
                        Tacit Ceramics (2019). </p>
                </div>
                <div class="col-lg-4">
                    <div class="item zoom-on-hover">
                        <a class="lightbox" href="assets/img/aset/SharingSession.jpg">
                            <img class="img-fluid image-atas" src="assets/img/aset/SharingSession.jpg">
                        </a>
                    </div>
                    <div class="h4 mb-3 mt-3" id="judul">XIXIXI</div>
                    <p class="card-text" id="kiri">Rakarsa dan Nuart Sculpture Park menginisiasi XIXIXI pada tahun 2020
                        sebagai ajang diskusi seni dalam suasana ringan dalam merespon pandemi Covid-19. Setiap minggu,
                        seniman, kurator, pengelola seni dan peneliti diundang untuk berbagi pengetahuan dalam bentuk
                        wicara
                        maupun lokakarya. Topik yang diangkat dari mulai apresiasi seni, menulis lagu untuk anak-anak
                        hingga
                        budaya visual seperti fenomena K-Pop. </p>
                    <p class="card-text" id="kiri">Inisiatif lainnya adalah Residency Sharing Session bersama Meicy
                        Sitorus,
                        M. Sabil (2019) dan Eksperimen Nonton Film (2018) yang dilengkapi dengan diskusi film</p>
                    <p class="card-text" id="kanan">Initiated by Rakarsa and Nuart Sculpture Park in 2020, XIXIXI
                        (informal
                        laughing expression of chatting) gathers people to discuss and make art in a lighter manner
                        during
                        Covid-19 pandemic. Every weekend, artists, curators, art managers and researchers were invited
                        to
                        share their knowledge in forms of talk or workshop. Topics range from art appreciation, children
                        song writing, to visual culture such as K-Pop phenomenon. </p>
                    <p class="card-text" id="kanan">Other initiatives are Residency Sharing Session with Meicy Sitorus,
                        M.
                        Sabil (2019) and Eksperimen Nonton Film (2018), a curative movie screening followed by
                        discussion.
                    </p>
                </div>
                <div class="col-lg-4">
                    <div class="item zoom-on-hover">
                        <a class="lightbox" href="assets/img/aset/Rakarsa_Kredit-Ardiles Klimarsen-3.jpg">
                            <img class="img-fluid image-atas"
                                src="assets/img/aset/Rakarsa_Kredit-Ardiles Klimarsen-3.jpg" alt="Art Unlimited">
                        </a>
                    </div>
                    <div class="h4 mb-3 mt-3" id="judul">Art Unlimited</div>
                    <p class="card-text" id="kiri">Art Unlimited adalah platform distribusi pasar bagi seniman muda yang
                        didukung oleh Badan Ekonomi Kreatif (sekarang Kemenparekraf). Art Unlimited 1 dan 2 diadakan
                        pada
                        Art Jakarta Art Fair di 2018-2019. Acara ini juga menjadi sarana eksplorasi manajemen dan riset
                        penerimaan pasar terhadap medium pilihan seniman muda.</p>
                    <p class="card-text" id="kiri">Inisiatif lainnya adalah pengelolaan pameran bersama seniman muda
                        ‘Nodes’
                        di Ruang Dini (2020). bersama beberapa kolektif seni Bandung, serta kolaborasi dengan Art Dept
                        untuk
                        menampilkan seniman muda pada Brightspot Market (2019).</p>
                    <p class="card-text" id="kanan">Art Unlimited is a platform of artwork market distribution for young
                        artists. Supported by Badan Ekonomi Kreatif (now a part of Kemenparekraf), Art Unlimited 1 and 2
                        were held in Art Jakarta Art Fair in 2018-2019. The event was also exercised to gain insight of
                        art
                        market's medium acceptance of young artists' artworks.</p>
                    <p class="card-text" id="kanan">Other initiatives are young artists showcase initiative ‘Nodes’ at
                        Ruang
                        Dini (2020) with other Bandung art collectives; and collaboration with Art Dept to showcase
                        young
                        artists in Brightspot Market (2019).</p>
                </div>
        </section>
    </div>
    <!-- collaborative artworks -->
    <div class="container">
        <div class="h4 pt-5 pb-3" id="judul">collaborative artworks</div>
        <section class="gallery-block compact-gallery">
            <div class="row no-gutters">
                <div class="col-md-6 col-lg-4 item zoom-on-hover">
                    <a class="lightbox" href="assets/img/aset/Rakarsa-AquaDanone-2.jpg">
                        <img class="img-fluid image" src="assets/img/aset/Rakarsa-AquaDanone-2.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item zoom-on-hover">
                    <a class="lightbox" href="assets/img/aset/Rakarsa-AquaDanone-1.jpg">
                        <img class="img-fluid image" src="assets/img/aset/Rakarsa-AquaDanone-1.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <span class="description">
                        <p class="font-weight-bold">
                            Recycled Plastic Installation for Aqua New Life Exhibition, Gandaria City Jakarta (2019).
                            </br>
                        </p>
                        <span>Artists:</br></span>
                        <span>Alfiah Rahdini & Kolasa Workshop</span>
                    </span>
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item zoom-on-hover">
                    <a class="lightbox" href="assets/img/aset/Rakarsa-Gojek-1.jpg">
                        <img class="img-fluid image" src="assets/img/aset/Rakarsa-Gojek-1.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4 item zoom-on-hover">
                    <a class="lightbox" href="assets/img/aset/Rakarsa-Gojek-2.jpg">
                        <img class="img-fluid image" src="assets/img/aset/Rakarsa-Gojek-2.jpg">
                    </a>
                </div>
                <div class="col-md-6 col-lg-4">
                    <span class="description">
                        <p class="font-weight-bold">
                            Light and Bamboo Installation for Gojek Food Festival at GBK, Jakarta (2019).</br>
                        </p>
                        <span>Artists:</br></span>
                        <span>Kolasa Workshop </span>
                    </span>
                </div>
            </div>
        </section>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
        <script>
        baguetteBox.run('.compact-gallery', {
            animation: 'slideIn'
        });
        </script>
    </div>
    <?php include ('template/footer.php'); ?>
</body>

<!-- <div class="container gallery-container">
            <div class="tz-gallery">
                <div class="row">
                    <div class="col-sm-12 col-md-3">
                        <a class="lightbox" href="assets/img/aset/Rakarsa-AquaDanone-2.jpg">
                            <img src="assets/img/aset/Rakarsa-AquaDanone-2.jpg" alt="AquaDanone2">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <a class="lightbox" href="assets/img/aset/Rakarsa-AquaDanone-2.jpg">
                            <img src="assets/img/aset/Rakarsa-AquaDanone-1.jpg" alt="AquaDanone1">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <p>
                            Recycled Plastic Installation for Aqua New Life Exhibition, Gandaria City Jakarta (2019).
                        </p>
                        <p>Artists:</br>
                            Alfiah Rahdini & Kolasa Workshop</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <a class="lightbox" href="assets/img/aset/Rakarsa-Gojek-1.jpg">
                            <img src="assets/img/aset/Rakarsa-Gojek-1.jpg" alt="AquaDanone">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <a class="lightbox" href="assets/img/aset/Rakarsa-Gojek-2.jpg">
                            <img src="assets/img/aset/Rakarsa-Gojek-2.jpg" alt="AquaDanone">
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <p>
                            Light and Bamboo Installation for Gojek Food Festival at GBK, Jakarta (2019).
                        </p>
                        <p>Artists:</br>
                            Kolasa Workshop</p>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
        <script>
        baguetteBox.run('.tz-gallery');
        </script> -->